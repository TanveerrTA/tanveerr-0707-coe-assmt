import React, {Component} from 'react';
import '../Styles/Header.css';

class PageHeader extends Component {
    // const[amount,setAmount] = useState(0)
    constructor(props){
        super(props);
        this.state ={
            amount : 0,
            inputValue:0,
        }
    }
    updateAmount(e){
        this.setState({
            amount:this.state.inputValue
        })
        this.props.amount(this.state.inputValue)
        e.preventDefault()
    }
    handleChange(e){
        // setAmount(e.target.value)
        this.setState({
            inputValue : e.target.value
        })
        
    }
    render(){
        return(
            <>
                <div className="page-header-style">
                    <div className="page-header-name">
                        Enter Allocated Total Amount:
                    </div>
                    <input type="number" className="main-input-box"  onInput value={this.state.inputValue} onChange={this.handleChange.bind(this)}  />
                    <button className="button-style"  onClick={this.updateAmount.bind(this)} >SUBMIT </button>
                </div>
            </>
        )
    }
}
// export {MyContextConsumer};
export default PageHeader;