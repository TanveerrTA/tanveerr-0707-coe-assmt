import React from 'react';
import '../Styles/Header.css';
import Fund from '../Images/fund.png';

const Header= props => {
        return (
        <div className="top-header">
            <div className=""></div>
            <img src={Fund} alt="fund" className="fund-logo" />
            <div className="header-alignment">
                FUND ALLOCATION
            </div>
        </div>
        );
    
}


export default Header;
