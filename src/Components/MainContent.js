/* eslint-disable no-lone-blocks */
import React,{Component} from 'react';
import '../Styles/mainContent.css';
import Arrow from '../Images/arrow.png';
// import { maxHeaderSize } from 'http';

class MainContent extends Component{
    constructor(props){
        super(props);
        this.state = {
            constantValue:{"Marketing & Sales":50,"Marketing":25,"Digital Marketing":25,"Sales":25,"Finance":25,"Inhouse expenses":15,"Tax Audit":10,"Operation & Maintenance":25,"Operation":13,"Internal Operation":13,"Maintenance":12},            
            relationshipData:{"Marketing & Sales":{"Marketing":{"Digital Marketing":{}},"Sales":{}},"Finance":{"Inhouse expenses":{},"Tax Audit":{}},"Operation & Maintenance":{"Operation":{"Internal Operation":{}},"Maintenance":{}}},
            dataValue:{"Marketing & Sales":0,"Marketing":0,"Digital Marketing":0,"Sales":0,"Finance":0,"Inhouse expenses":0,"Tax Audit":0,"Operation & Maintenance":0,"Operation":0,"Maintenance":0},
            showSecondLevel:false,
            showThirdLevel:false,
        }
        
    }

    componentDidMount(){
        this.setState({
            relationshipData:{"Marketing & Sales":{"Marketing":{"Digital Marketing":{}},"Sales":{}},"Finance":{"Inhouse expenses":{},"Tax Audit":{}},"Operation & Maintenance":{"Operation":{"Internal Operation":{}},"Maintenance":{}}},
            dataValue:{"Marketing & Sales":50,"Marketing":25,"Digital Marketing":25,"Sales":25,"Finance":25,"Inhouse expenses":15,"Tax Audit":10,"Operation & Maintenance":25,"Operation":13,"Internal Operation":13,"Maintenance":12},
        });
    };
    
    
    changeFirstLevel(itemLevel1,e){
        
        var newValue = e.target.value;
        const prevDataValue = {...this.state.constantValue};
        var dataValue = this.state.dataValue;
        var relationshipData = this.state.relationshipData;
        if(newValue>100){
            newValue=100
        }
        else if(newValue<0 || newValue===''){
            newValue=0
        }
        var prevValue = dataValue[itemLevel1]
        var changedValue = newValue-prevValue;
        var countLevel1 = Object.keys(relationshipData).length -1
        Object.keys(relationshipData).map((dataLevel1)=>{
            if(dataLevel1 === itemLevel1){
                
                dataValue[itemLevel1]=Math.round(parseFloat(newValue)*100)/100
                Object.keys(relationshipData[dataLevel1]).map((dataLevel2)=>{
                    dataValue[dataLevel2]=Math.round((parseFloat(dataValue[dataLevel1])*parseFloat(prevDataValue[dataLevel2])/parseFloat(prevDataValue[dataLevel1]))*100)/100
                    {Object.keys(relationshipData[dataLevel1][dataLevel2]).length>0 && Object.keys(relationshipData[dataLevel1][dataLevel2]).map((dataLevel3)=>{
                        dataValue[dataLevel3]=Math.round((parseFloat(dataValue[dataLevel2])*parseFloat(prevDataValue[dataLevel3])/parseFloat(prevDataValue[dataLevel2]))*100)/100
                    })} 
                    
                })
            }
            else{
                
                if(parseInt(newValue)===100){
                    dataValue[dataLevel1]=0
                    Object.keys(relationshipData[dataLevel1]).map((dataLevel2)=>{
                        dataValue[dataLevel2]=0
                        {Object.keys(relationshipData[dataLevel1][dataLevel2]).length>0 && Object.keys(relationshipData[dataLevel1][dataLevel2]).map((dataLevel3)=>{
                            dataValue[dataLevel3]=0
                        })}
                    })
                }
                else{
                    dataValue[dataLevel1] = Math.round((parseFloat(dataValue[dataLevel1]) - parseFloat(changedValue)/countLevel1)*100)/100
                    
                    if(dataValue[dataLevel1]<0 ){
                        dataValue[dataLevel1]=0
                        Object.keys(relationshipData).map((item)=>{
                            if(item!==dataLevel1 && item!==itemLevel1){
                                dataValue[item] = Math.round((parseFloat(dataValue[item]) - parseFloat(changedValue)/countLevel1)*100)/100
                                Object.keys(relationshipData[item]).map((dataLevel2)=>{
                                    dataValue[dataLevel2]=Math.round((parseFloat(dataValue[item])*parseFloat(prevDataValue[dataLevel2])/parseFloat(prevDataValue[item]))*100)/100
                                    {Object.keys(relationshipData[item][dataLevel2]).length>0 && Object.keys(relationshipData[item][dataLevel2]).map((dataLevel3)=>{
                                        dataValue[dataLevel3]=Math.round((parseFloat(dataValue[dataLevel2])*parseFloat(prevDataValue[dataLevel3])/parseFloat(prevDataValue[dataLevel2]))*100)/100 
                                    })} 
                                })
                            }
                        })
                    }
                    Object.keys(relationshipData[dataLevel1]).map((dataLevel2)=>{
                        dataValue[dataLevel2]=Math.round((parseFloat(dataValue[dataLevel1])*parseFloat(prevDataValue[dataLevel2])/parseFloat(prevDataValue[dataLevel1]))*100)/100
                        {Object.keys(relationshipData[dataLevel1][dataLevel2]).length>0 && Object.keys(relationshipData[dataLevel1][dataLevel2]).map((dataLevel3)=>{
                            dataValue[dataLevel3]=Math.round((parseFloat(dataValue[dataLevel2])*parseFloat(prevDataValue[dataLevel3])/parseFloat(prevDataValue[dataLevel2]))*100)/100
                        })} 
                    })
                }
            }
        })
        
        


        this.setState({
            dataValue:dataValue
        })

    }
    changeSecondLevel(itemLevel1,itemLevel2,e){
        var newValue = e.target.value;
        const prevDataValue = {...this.state.constantValue};
        var dataValue = this.state.dataValue;
        var relationshipData = this.state.relationshipData;
        if(newValue>100){
            newValue=100
        }
        else if(newValue<0 || newValue===''){
            newValue=0
        }
        var prevValue = dataValue[itemLevel2]
        var changedValue = newValue-prevValue;
        dataValue[itemLevel2] = Math.round(parseFloat(newValue)*100)/100
        if(parseInt(newValue)===100){
            dataValue[itemLevel1] = 100
            Object.keys(relationshipData[itemLevel1]).map((dataLevel2)=>{
                if(dataLevel2!==itemLevel2){
                    dataValue[dataLevel2]=0
                    {Object.keys(relationshipData[itemLevel1][dataLevel2]).length>0 && Object.keys(relationshipData[itemLevel1][dataLevel2]).map((dataLevel3)=>{
                        dataValue[dataLevel3]=0
                    })}
                }
                else{
                    {Object.keys(relationshipData[itemLevel1][itemLevel2]).length>0 && Object.keys(relationshipData[itemLevel1][itemLevel2]).map((dataLevel3)=>{
                        dataValue[dataLevel3]=Math.round((parseFloat(dataValue[itemLevel2])*parseFloat(prevDataValue[dataLevel3])/parseFloat(prevDataValue[itemLevel2]))*100)/100
                    })}
                }
            })
            
        }
        else if(parseInt(dataValue[itemLevel1])>=100){
            dataValue[itemLevel1]=100
            Object.keys(relationshipData[itemLevel1]).map((dataLevel2)=>{
                if(dataLevel2!==itemLevel2){
                    var countLevel2 = Object.keys(relationshipData[itemLevel1]).length-1
                    dataValue[dataLevel2] = Math.round((parseFloat(dataValue[dataLevel2])-parseFloat(changedValue)/countLevel2)*100)/100
                    
                }
                {Object.keys(relationshipData[itemLevel1][dataLevel2]).length>0 && Object.keys(relationshipData[itemLevel1][dataLevel2]).map((dataLevel3)=>{
                    dataValue[dataLevel3]=Math.round((parseFloat(dataValue[dataLevel2])*parseFloat(prevDataValue[dataLevel3])/parseFloat(prevDataValue[dataLevel2]))*100)/100
                })}
            })
        }
        else{
            dataValue[itemLevel1] = Math.round((parseFloat(dataValue[itemLevel1])+parseFloat(changedValue))*100)/100
            {Object.keys(relationshipData[itemLevel1][itemLevel2]).length>0 && Object.keys(relationshipData[itemLevel1][itemLevel2]).map((dataLevel3)=>{
                dataValue[dataLevel3]=Math.round((parseFloat(dataValue[itemLevel2])*parseFloat(prevDataValue[dataLevel3])/parseFloat(prevDataValue[itemLevel2]))*100)/100
            })}
        }
        
        var countLevel1 = Object.keys(relationshipData).length -1

        Object.keys(relationshipData).map((dataLevel1)=>{
            if(dataLevel1!==itemLevel1){
                if(parseInt(dataValue[itemLevel1])===100){
                    dataValue[dataLevel1]=0
                    Object.keys(relationshipData[dataLevel1]).map((dataLevel2)=>{
                        dataValue[dataLevel2]=0
                        {Object.keys(relationshipData[dataLevel1][dataLevel2]).length>0 && Object.keys(relationshipData[dataLevel1][dataLevel2]).map((dataLevel3)=>{
                            dataValue[dataLevel3]=0
                        })}
                    })
                }
                else{
                    dataValue[dataLevel1] = Math.round((parseFloat(dataValue[dataLevel1]) - parseFloat(changedValue)/countLevel1)*100)/100
                    if(dataValue[dataLevel1]<0 ){
                        dataValue[dataLevel1]=0
                        Object.keys(relationshipData).map((item)=>{
                            if(item!==dataLevel1 && item!==itemLevel1){
                                dataValue[item] = Math.round((parseFloat(dataValue[item]) - parseFloat(changedValue)/countLevel1)*100)/100
                                Object.keys(relationshipData[item]).map((dataLevel2)=>{
                                    dataValue[dataLevel2]=Math.round((parseFloat(dataValue[item])*parseFloat(prevDataValue[dataLevel2])/parseFloat(prevDataValue[item]))*100)/100
                                    {Object.keys(relationshipData[item][dataLevel2]).length>0 && Object.keys(relationshipData[item][dataLevel2]).map((dataLevel3)=>{
                                        dataValue[dataLevel3]=Math.round((parseFloat(dataValue[dataLevel2])*parseFloat(prevDataValue[dataLevel3])/parseFloat(prevDataValue[dataLevel2]))*100)/100
                                    })} 
                                })
                            }
                        })
                    }
                    Object.keys(relationshipData[dataLevel1]).map((dataLevel2)=>{
                        dataValue[dataLevel2]=Math.round((parseFloat(dataValue[dataLevel1])*parseFloat(prevDataValue[dataLevel2])/parseFloat(prevDataValue[dataLevel1]))*100)/100
                        {Object.keys(relationshipData[dataLevel1][dataLevel2]).length>0 && Object.keys(relationshipData[dataLevel1][dataLevel2]).map((dataLevel3)=>{
                            dataValue[dataLevel3]=Math.round((parseFloat(dataValue[dataLevel2])*parseFloat(prevDataValue[dataLevel3])/parseFloat(prevDataValue[dataLevel2]))*100)/100
                        })} 
                    })
                }
            }
        })
        this.setState({
            dataValue:dataValue
        })

    }
    
    changeThirdLevel(itemLevel1,itemLevel2,itemLevel3,e){
        var newValue = e.target.value;
        const prevDataValue = {...this.state.constantValue};
        var dataValue = this.state.dataValue;
        var relationshipData = this.state.relationshipData;
        if(newValue>100){
            newValue=100
        }
        else if(newValue<0 || newValue===''){
            newValue=0
        }
        var countLevel1 = Object.keys(relationshipData).length -1

        var prevValue = dataValue[itemLevel3]
        var changedValue = newValue-prevValue;
        dataValue[itemLevel3] = newValue
        if(parseInt(newValue)===100){
            dataValue[itemLevel2] = 100
            dataValue[itemLevel1] = 100
            Object.keys(relationshipData[itemLevel1]).map((dataLevel2)=>{
                if(dataLevel2!==itemLevel2){
                    dataValue[dataLevel2]=0
                }
                {Object.keys(relationshipData[itemLevel1][itemLevel2]).length>0 && Object.keys(relationshipData[itemLevel1][itemLevel2]).map((dataLevel3)=>{
                    dataValue[dataLevel3]=Math.round((parseFloat(dataValue[itemLevel2])*parseFloat(prevDataValue[dataLevel3])/parseFloat(prevDataValue[itemLevel2]))*100)/100
                })}
            })
            
        }
        else if(parseInt(dataValue[itemLevel1])>=100){
            dataValue[itemLevel1]=100
            dataValue[itemLevel2] = Math.round((parseFloat(dataValue[itemLevel2])+parseFloat(changedValue))*100)/100
            Object.keys(relationshipData[itemLevel1]).map((dataLevel2)=>{
                if(dataLevel2!==itemLevel2){
                    var countLevel2 = Object.keys(relationshipData[itemLevel1]).length-1
                    dataValue[dataLevel2] = Math.round((parseFloat(dataValue[dataLevel2])-parseFloat(changedValue)/countLevel2)*100)/100
                    {Object.keys(relationshipData[itemLevel1][dataLevel2]).length>0 && Object.keys(relationshipData[itemLevel1][dataLevel2]).map((dataLevel3)=>{
                        dataValue[dataLevel3]=Math.round((parseFloat(dataValue[dataLevel2])*parseFloat(prevDataValue[dataLevel3])/parseFloat(prevDataValue[dataLevel2]))*100)/100
                    })}
                }
            })
        }
        else{
            dataValue[itemLevel2] = Math.round((parseFloat(dataValue[itemLevel2])+parseFloat(changedValue))*100)/100
            dataValue[itemLevel1] = Math.round((parseFloat(dataValue[itemLevel1])+parseFloat(changedValue))*100)/100
        }

        Object.keys(relationshipData).map((dataLevel1)=>{
            if(dataLevel1!==itemLevel1){
                if(dataValue[itemLevel1]===100){
                    dataValue[dataLevel1]=0
                    Object.keys(relationshipData[dataLevel1]).map((dataLevel2)=>{
                        dataValue[dataLevel2]=0
                        {Object.keys(relationshipData[dataLevel1][dataLevel2]).length>0 && Object.keys(relationshipData[dataLevel1][dataLevel2]).map((dataLevel3)=>{
                            dataValue[dataLevel3]=0
                        })}
                    })
                }
                else{
                    dataValue[dataLevel1] = Math.round((parseFloat(dataValue[dataLevel1]) - parseFloat(changedValue)/countLevel1)*100)/100
                    if(dataValue[dataLevel1]<0 ){
                        dataValue[dataLevel1]=0
                        Object.keys(relationshipData).map((item)=>{
                            if(item!==dataLevel1 && item!==itemLevel1){
                                dataValue[item] = Math.round((parseFloat(dataValue[item]) - parseFloat(changedValue)/countLevel1)*100)/100
                                Object.keys(relationshipData[item]).map((dataLevel2)=>{
                                    dataValue[dataLevel2]=Math.round((parseFloat(dataValue[item])*parseFloat(prevDataValue[dataLevel2])/parseFloat(prevDataValue[item]))*100)/100
                                    {Object.keys(relationshipData[item][dataLevel2]).length>0 && Object.keys(relationshipData[item][dataLevel2]).map((dataLevel3)=>{
                                        dataValue[dataLevel3]=Math.round((parseFloat(dataValue[dataLevel2])*parseFloat(prevDataValue[dataLevel3])/parseFloat(prevDataValue[dataLevel2]))*100)/100
                                    })} 
                                })
                            }
                        })
                    }
                    Object.keys(relationshipData[dataLevel1]).map((dataLevel2)=>{
                        dataValue[dataLevel2]=Math.round((parseFloat(dataValue[dataLevel1])*parseFloat(prevDataValue[dataLevel2])/parseFloat(prevDataValue[dataLevel1]))*100)/100
                        {Object.keys(relationshipData[dataLevel1][dataLevel2]).length>0 && Object.keys(relationshipData[dataLevel1][dataLevel2]).map((dataLevel3)=>{
                            dataValue[dataLevel3]=Math.round((parseFloat(dataValue[dataLevel2])*parseFloat(prevDataValue[dataLevel3])/parseFloat(prevDataValue[dataLevel2]))*100)/100
                        })} 
                    })
                }
            }
        })
        this.setState({
            dataValue:dataValue
        })
    }





    render() {
        const value = this.props.amount
        return(
            <>
                {this.props.isButtonClicked?
                <>
                <div className="button-content-style">
                    <button  className="button-style" onClick={()=>this.setState({showSecondLevel:!this.state.showSecondLevel,showThirdLevel:false})}>{this.state.showSecondLevel?"Hide Second Level":"Show Second Level"}</button>
                    <button className="button-style" onClick={()=>this.setState({showThirdLevel:!this.state.showThirdLevel})} disabled={!this.state.showSecondLevel}>{this.state.showThirdLevel?"Hide Third Level":"Show Third Level"}</button>
                </div>
                <div className="main-content-style">
                    <div className="heading">
                        <div style={{flex:"0 0 60%"}}>Divisions</div>
                        <div style={{flex:"0 0 31%"}}>Percentage(%)</div>
                        <div style={{flex:1}}>Amount</div>
                    </div>
                    {Object.keys(this.state.relationshipData).map((itemLevel1)=>{
                        return(
                            <div>
                                <div className="parent-division">
                                    {/* <img src={Arrow} alt="arrow" /> */}
                                    <div className = "parent-division-text">{itemLevel1}</div>
                                    <input type="number" max={100} className="parent-division-percent" onChange={this.changeFirstLevel.bind(this,itemLevel1)} value={this.state.dataValue[itemLevel1]} />
                                    <div style={{flex:1}}/>
                                    <div className="parent-division-amount">{Math.round((value*this.state.dataValue[itemLevel1]/100)*100)/100}</div>
                                </div>
                                {this.state.relationshipData[itemLevel1]!=={} && Object.keys(this.state.relationshipData[itemLevel1]).map((itemLevel2)=>{
                                  return(
                                        <div>
                                            <div className={this.state.showSecondLevel?"second-division":"hide-second-division"}>
                                                {/* <img src={Arrow} alt="arrow" /> */}
                                                <div className = "second-division-text">{itemLevel2}</div>
                                                <input type="number" max={100} min={0} className="parent-division-percent" onChange={this.changeSecondLevel.bind(this,itemLevel1,itemLevel2)} value={this.state.dataValue[itemLevel2]} />                   
                                                <div style={{flex:1}}/>
                                                <div className="second-division-amount">{Math.round((value*this.state.dataValue[itemLevel2]/100)*100)/100}</div>
                                            </div>
                                            {this.state.relationshipData[itemLevel1][itemLevel2]!=={} && Object.keys(this.state.relationshipData[itemLevel1][itemLevel2]).map((itemLevel3)=>{
                                                return(
                                                    <div className={this.state.showThirdLevel && this.state.showSecondLevel?"third-division":"hide-third-division"}>
                                                        {/* <img src={Arrow} alt="arrow" /> */}
                                                        <div className = "third-division-text">{itemLevel3}</div>
                                                        <input type="number" max={100} className="parent-division-percent" onChange={this.changeThirdLevel.bind(this,itemLevel1,itemLevel2,itemLevel3)} value={this.state.dataValue[itemLevel3]} />                                             
                                                        <div style={{flex:1}}/>
                                                        <div className="third-division-amount">{Math.round((value*this.state.dataValue[itemLevel3]/100)*100)/100}</div>
                                                    </div>
                                                )
                                            })}
                                        </div>
                                  )  
                                })}
                            </div>
                        )
                    })}
                    </div>
                </>
                
                :
                <div className="main-content-style">
                    <div className="blank-screen-text">PLEASE ENTER ALLOCATED AMOUNT</div>
                </div>
                }
                </>
        )
    }
}
export default MainContent;


