import React,{Component} from 'react';
import PageHeader from './PageHeader';
import MainContent from './MainContent';

class Home extends Component{
    constructor(props){
        super(props);
        this.state = {
            amount : 0,
            refresh:false,
            isButtonClicked:false,
        }
        this.amount = this.amount.bind(this)
    }

    amount(val){
        this.setState({
            amount:val,
            refresh:true,
            isButtonClicked:true,
        })
    }
    render(){
        return (
            <div>
                <PageHeader amount={this.amount} />
                <MainContent amount={this.state.amount} isButtonClicked={this.state.isButtonClicked} refresh={this.state.refresh} />
            </div>
        );
    }
}
export default Home;